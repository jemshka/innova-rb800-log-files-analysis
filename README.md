# Innova RB800 log files analysis

## Background

In April 2022, a surveillance device was discovered inside the printer of the anarchist library Libertad in Paris, France. The discovery was announced [on the library website (in french)](https://bibliothequelibertad.noblogs.org/post/2022/04/02/un-micro-trouve-a-la-bibliotheque-anarchiste-libertad). The device, based on the RB800 device sold by italian surveillance company *Innova*, was capable of recording audio with its microphone, store the recorded data on its SD card, and transmit it over the mobile phone network.

The device SD card contained log files detailing its activity for ~6 weeks prior to the discovery.

## rb800-log

*rb800-log* was written to analyse the log files found on the device SD card. It outputs a text report in Markdown, as well as graphical charts of the device activity.

Presumably, *rb800-log* could be used to analyze log files of other RB800 devices, and perhaps of other devices sold by *Innova*. If you have any success using this script, I'd like to know! You can send an email to jemshka@riseup.net.

## Dependencies

Python >= 3.9, Matplotlib (matplotlib.org)

## Usage

For usage information, run:

`python3 rb800-log/rb800-log.py -h`
