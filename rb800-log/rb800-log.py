import argparse
import datetime
import enum
import json
import matplotlib.pyplot as plt
import os
import re
import shutil
import sys

# Load strings
with open(os.path.join(sys.path[0], "strings.json"), "r") as strings_file:
    STRINGS = json.loads(strings_file.read())

# Converts a timedelta to a string in the given language
def timedelta_to_string(timedelta, lang):
    periods = [
        ("year", 60*60*24*365),
        ("month", 60*60*24*30),
        ("day", 60*60*24),
        ("hour", 60*60),
        ("minute", 60),
        ("second", 1)
    ]
    seconds = int(timedelta.total_seconds())
    strings = []
    for period_id, period_seconds in periods:
        if seconds >= period_seconds:
            period_value, seconds = divmod(seconds, period_seconds)
            period_name = STRINGS["strings"][f"period_{period_id}"][lang]
            period_plural = STRINGS["strings"][f"period_plural_{period_id}"][lang]
            strings.append(f"{period_value} {period_plural if period_value > 1 else period_name}")
    return ", ".join(strings)

# Removes a prefix from a string
def remove_prefix(string, prefix):
    if string.startswith(prefix):
        return string[len(prefix):]
    return string

# Represents a log line from log files of a RB800 device
class LogLine:
    class IPType(enum.Enum):
        TARGET = enum.auto()
        DEVICE = enum.auto()
        SNTP = enum.auto()

        def description(self, lang):
            if self == self.TARGET:
                return STRINGS["strings"]["ip_description_target"][lang]
            elif self == self.DEVICE:
                return STRINGS["strings"]["ip_description_device"][lang]
            elif self == self.SNTP:
                return STRINGS["strings"]["ip_description_sntp"][lang]

    DATE_TIME = re.compile(r'^(\d\d)\:(\d\d)\:(\d\d) (\d\d)/(\d\d)/(\d\d)(.*)')
    DEVICE_IP = re.compile(r'IP \= (\d+\.\d+\.\d+\.\d+)')
    PHONE_NUMBERS = [
        re.compile(r'^ST RX \- \+CMGR\: \"REC UNREAD\"\,\"(\+?\d+)\"'),
        re.compile(r'^Rx SMS da (\+?\d+)'),
        re.compile(r'^N\. al assente (\+?\d+)'),
        re.compile(r'^ST TX \- AT\+CMGS\=\"(\+?\d+)\"'),
        re.compile(r'^ST RX \- \+CPBR\: 1\,\"(\+?\d+)\"')
    ]
    SPEED = re.compile(r'(\d+\.\d+) KB\/s')
    SNTP_IP = re.compile(r'^SNTP\..* (\d+\.\d+\.\d+\.\d+)')
    TARGET_IP = re.compile(r'Server (\d+\.\d+\.\d+\.\d+) \d+')
    TARGET_AND_DEVICE_IPS = re.compile(r'Conn\. server (\d+\.\d+\.\d+\.\d+) \d+ da (\d+\.\d+\.\d+\.\d+)')
    TEMPERATURE = re.compile(r'T (\d+)C\, T incr\.')

    def __init__(self, line):
        # Set line
        self.line = line.strip("\n")

        # Set datetime and content
        self.datetime = None
        match = self.DATE_TIME.search(self.line)
        if match:
            self.datetime = datetime.datetime(
                2000 + int(match.group(6)),
                int(match.group(5)),
                int(match.group(4)),
                int(match.group(1)),
                int(match.group(2)),
                int(match.group(3))
            )
            self.content = remove_prefix(match.group(7), " - ")
        else:
            self.content = self.line

        # Set temperature
        self.temperature = None
        match = self.TEMPERATURE.search(self.content)
        if match:
            self.temperature = int(match.group(1))

        # Set transfer speed to the highest speed mentioned in the content
        self.transfer_speed = None
        for speed in re.findall(self.SPEED, self.content):
            float_speed = float(speed)
            if not self.transfer_speed or float_speed > self.transfer_speed:
                self.transfer_speed = float_speed

        # Create IPs
        self.ips = dict()

        # Set target and device IPs
        match = self.TARGET_AND_DEVICE_IPS.search(self.content)
        if match:
            self.ips[self.IPType.TARGET] = match.group(1)
            self.ips[self.IPType.DEVICE] = match.group(2)
        else:
            match = self.TARGET_IP.search(self.content)
            if match:
                self.ips[self.IPType.TARGET] = match.group(1)
            match = self.DEVICE_IP.search(self.content)
            if match:
                self.ips[self.IPType.DEVICE] = match.group(1)

        # Set SNTP IP
        match = self.SNTP_IP.search(self.content)
        if match:
            self.ips[self.IPType.SNTP] = match.group(1)

        # Set phone number
        self.phone_number = None
        for phone_number_regex in self.PHONE_NUMBERS:
            match = phone_number_regex.search(self.content)
            if match:
                self.phone_number = match.group(1)
                break

    # Returns the number of minutes between the start of the day and the line datetime
    def minutes_since_midnight(self):
        if self.datetime != None:
            time_delta = self.datetime - datetime.datetime(self.datetime.year, self.datetime.month, self.datetime.day)
            return time_delta.seconds // 60
        raise RuntimeError("Called minutes_since_midnight() on an undated log line.")

    # Returns True if the line shuts down the device
    def shutdown(self):
        return self.content == "rm300 OFF"

    # Returns True if the line turns the voice activation off
    def voice_activation_off(self):
        return self.content.startswith("VOX_Off")

    # Returns True if the line turns the voice activation on
    def voice_activation_on(self):
        return self.content.startswith("VOX_On")

# Wrapper class used to perform the analysis of log files of a RB800 device
class LogAnalysis:
    def __init__(self, input_folder, output_folder, lang, first_day = None, last_day = None, tmp_folder = None):
        # Set attributes
        self.input_folder = input_folder
        self.output_folder = output_folder
        self.lang = lang
        self.first_day = first_day
        self.last_day = last_day
        self.tmp_folder = tmp_folder

        # If no tmp folder is set, it defaults to the output folder
        if self.tmp_folder == None:
            self.tmp_folder = self.output_folder

    # Generates data that can be used with matplotlib's broken_barh graph
    # Minutes elapsed between a log line that fits the start criteria and one that fits the end criteria will be colored
    # Returns a tuple containing the data to give to matplotlib's broken_barh function, and a boolean that indicates if the criteria is being satisfied at the end of the day
    def generate_daily_broken_barh_data(self, log_lines, start_criteria, end_criteria, start_colored):
        data = []
        if start_colored:
            minutes = 0
        else:
            minutes = None  
        for line in log_lines:
            if minutes == None:
                if start_criteria(line):
                    minutes = line.minutes_since_midnight()
            else:
                if end_criteria(line):
                    data.append((minutes, line.minutes_since_midnight() - minutes + 1))
                    minutes = None
        if minutes != None:
            data.append((minutes, 24 * 60 - minutes))
            return (data, True)
        return (data, False)

    # Generates transfer speed data that can be used with matplotlib's plot graph
    def generate_daily_transfer_speed_data(self, log_lines):
        x = []
        y = []
        for line in log_lines:
            if line.transfer_speed != None and line.transfer_speed > 0:
                x.append(line.minutes_since_midnight())
                y.append(line.transfer_speed)
        return (x, y)

    # Generates temperature data that can be used with matplotlib's plot graph
    def generate_daily_temperature_data(self, log_lines):
        data = []
        x = []
        y = []
        for line in log_lines:
            if line.shutdown():
                if x and y:
                    data.append((x, y))
                    x = []
                    y = []
            elif line.temperature != None:
                x.append(line.minutes_since_midnight())
                y.append(line.temperature)
        if x and y:
            data.append((x, y))
        return data

    # Returns a string in the right language
    def get_string(self, string_id):
        return STRINGS["strings"][string_id][self.lang]

    # Runs the analysis
    def run(self):
        # Delete and re-create the folder where daily logs will be created
        daily_logs_folder = os.path.join(self.tmp_folder, "rb800_analysis_tmp_daily_logs")
        if os.path.exists(daily_logs_folder):
            shutil.rmtree(daily_logs_folder)
        os.mkdir(daily_logs_folder)

        try:
            # Create log filenames
            log_filenames = []
            for filename in os.listdir(self.input_folder):
                if filename.endswith(".txt"):
                    log_filenames.append(os.path.join(self.input_folder, filename))

            # Print a message
            print(f"Parsing {len(log_filenames)} log file(s)...")

            # Parse the log lines, create the daily logs, and collect some other data
            daily_log_file = None
            dated_lines_number = 0
            dated_lines_not_included_number = 0
            dated_log_line_found = False
            days = []
            ips = dict()
            phone_numbers = dict()
            temperature_maximum = None
            temperature_minimum = None
            temperature_occurrences = 0
            temperature_total = 0
            transfer_speed_maximum = None
            undated_lines_number = 0
            for log_filename in log_filenames:
                with open(log_filename, "r") as log_file:
                    for line in log_file:
                        # Create the log line
                        log_line = LogLine(line)

                        # If the line is dated
                        if log_line.datetime != None:
                            # A dated log line was found
                            dated_log_line_found = True

                            # If the line date is included in the analysis
                            if (self.first_day == None or log_line.datetime.date() >= self.first_day) and \
                               (self.last_day == None or log_line.datetime.date() <= self.last_day):
                                # Open the corresponding daily log file
                                day = log_line.datetime.date()
                                daily_log_filename = f"{day}.txt"
                                if daily_log_file == None or os.path.basename(daily_log_file.name) != daily_log_filename:
                                    daily_log_file = open(os.path.join(daily_logs_folder, daily_log_filename), "a")
                                    if day not in days:
                                        days.append(day)

                                # Write the log line to the daily log file
                                daily_log_file.write(f"{log_line.line}\n")

                                # Update the temperature data
                                if log_line.temperature:
                                    temperature_occurrences += 1
                                    temperature_total += log_line.temperature
                                    if temperature_maximum == None or log_line.temperature > temperature_maximum:
                                        temperature_maximum = log_line.temperature
                                    if temperature_minimum == None or log_line.temperature < temperature_minimum:
                                        temperature_minimum = log_line.temperature

                                # Update the maximum transfer speed
                                if log_line.transfer_speed and (transfer_speed_maximum == None or log_line.transfer_speed > transfer_speed_maximum):
                                    transfer_speed_maximum = log_line.transfer_speed

                                # Collect the line IPs
                                for ip_type, ip in log_line.ips.items():
                                    ips.setdefault(ip_type, dict())
                                    ips[ip_type].setdefault(ip, 0)
                                    ips[ip_type][ip] += 1

                                # Collect the line phone numbers
                                if log_line.phone_number:
                                    phone_numbers.setdefault(log_line.phone_number, 0)
                                    phone_numbers[log_line.phone_number] += 1

                                # Count the line as a dated line
                                dated_lines_number += 1
                            # Else
                            else:
                                # Count the line as a dated line that was not included
                                dated_lines_not_included_number += 1
                        # Else
                        else:
                            # Count the line as an undated line
                            undated_lines_number += 1
            if daily_log_file != None:
                daily_log_file.close()

            # If there is at least one day included in the analysis
            if days:
                # Create the output folder
                if not os.path.isdir(self.output_folder):
                    os.mkdir(self.output_folder)

                # Sort the days
                days = sorted(days)

                # Compute the temperature average
                if temperature_occurrences > 0:
                    temperature_average = int(temperature_total / temperature_occurrences)

                # Sort the IPs
                for ip_type, ips_dict in ips.items():
                    ips[ip_type] = dict(sorted(ips_dict.items(), key = lambda item: item[1], reverse = True))

                # Create the charts folder
                charts_folder = os.path.join(self.output_folder, "charts")
                if not os.path.isdir(charts_folder):
                    os.mkdir(charts_folder)

                # Create the daily charts and collect data on the voice activation feature
                last_day_vox_on = False
                last_day_powered_off = False
                voice_activation_start = None
                voice_activation_minimum = None
                voice_activation_maximum = None
                voice_activation_total = datetime.timedelta()
                voice_activation_occurrences = 0
                for day_index, day in enumerate(days):
                    # Set the daily log filename
                    daily_log_filename = f"{day}.txt"

                    # Read the daily log
                    lines = []
                    with open(os.path.join(daily_logs_folder, daily_log_filename), "r") as daily_log_file:
                        lines = [LogLine(line) for line in daily_log_file]
                        lines = sorted(lines, key = lambda line: line.datetime)
                    day = lines[0].datetime.date()

                    # Set the first and last dated lines
                    if day_index == 0:
                        first_dated_line = lines[0]
                    if day_index == len(days) - 1:
                        last_dated_line = lines[-1]

                    # Collect data on the voice activation feature
                    for line in lines:
                        if voice_activation_start == None:
                            if line.voice_activation_on():
                                voice_activation_start = line.datetime
                        else:
                            if line.voice_activation_off() or line.shutdown():
                                voice_activation_duration = line.datetime - voice_activation_start
                                if voice_activation_minimum == None or voice_activation_duration < voice_activation_minimum:
                                    voice_activation_minimum = voice_activation_duration
                                if voice_activation_maximum == None or voice_activation_duration > voice_activation_maximum:
                                    voice_activation_maximum = voice_activation_duration
                                voice_activation_total += voice_activation_duration
                                voice_activation_occurrences += 1
                                voice_activation_start = None

                    # Create the figure and first ax
                    figure, first_ax = plt.subplots(figsize = (10, 7))

                    # First ax
                    first_ax.set_xlim(0, 60 * 24)
                    first_ax.set_xlabel(self.get_string("time_label"))
                    first_ax.set_xticks([hour * 60 for hour in [0, 6, 12, 18, 24]])
                    first_ax.set_xticklabels([f"{hour}:00" for hour in [0, 6, 12, 18, 24]])
                    first_ax.set_xticks([hour * 60 for hour in [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23]], minor = True)
                    first_ax.set_ylabel(self.get_string("temperature_label"))
                    first_ax.set_ylim(temperature_minimum - 1, temperature_maximum + 1)

                    # Second ax
                    second_ax = first_ax.twinx()
                    second_ax.set_ylabel(self.get_string("data_transmission_label"))
                    second_ax.set_ylim(0, transfer_speed_maximum + 1)

                    # Plot a broken_barh graph that represents when the device is powered off
                    data, last_day_powered_off = self.generate_daily_broken_barh_data(
                        lines,
                        lambda line: line.shutdown(),
                        lambda line: line.content.startswith("Accensione"),
                        last_day_powered_off
                    )
                    if data:
                        powered_off_broken_barh = first_ax.broken_barh(data, (first_ax.get_ylim()[0], first_ax.get_ylim()[1] - first_ax.get_ylim()[0]), facecolors = "#ff7c7c")
                    else:
                        powered_off_broken_barh = None

                    # Plot a broken_barh graphs that represents when the device was voice-activated
                    data, last_day_vox_on = self.generate_daily_broken_barh_data(
                        lines,
                        lambda line: line.voice_activation_on(),
                        lambda line: line.voice_activation_off() or line.shutdown(),
                        last_day_vox_on
                    )
                    if data:
                        voice_activation_broken_barh = first_ax.broken_barh(data, (first_ax.get_ylim()[0], first_ax.get_ylim()[1] - first_ax.get_ylim()[0]), facecolors = "silver")
                    else:
                        voice_activation_broken_barh = None

                    # Plot a bar graph that represents the device data transmission reports
                    data = self.generate_daily_transfer_speed_data(lines)
                    if data[0]:
                        data_transmission_plot = second_ax.plot(data[0], data[1], "b+")
                    else:
                        data_transmission_plot = None

                    # Plot the device temperature
                    temperature_plots = []
                    data = self.generate_daily_temperature_data(lines)
                    for x, y in data:
                        temperature_plots.append(first_ax.plot(x, y, color = "green"))

                    # Legend handles and labels
                    legend_handles = []
                    legend_labels = []
                    if temperature_plots:
                        legend_handles.append(temperature_plots[0][0])
                        legend_labels.append(self.get_string("temperature_legend"))
                    if data_transmission_plot:
                        legend_handles.append(data_transmission_plot[0])
                        legend_labels.append(self.get_string("data_transmission_legend"))
                    if voice_activation_broken_barh:
                        legend_handles.append(voice_activation_broken_barh)
                        legend_labels.append(self.get_string("voice_activation_legend"))
                    if powered_off_broken_barh:
                        legend_handles.append(powered_off_broken_barh)
                        legend_labels.append(self.get_string("powered_off_legend"))

                    # Legend
                    box = first_ax.get_position()
                    first_ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
                    first_ax.legend(legend_handles, legend_labels, loc = "upper left", bbox_to_anchor = (0, -0.12), fancybox = False)

                    # Title
                    plt.title(self.get_string("daily_analysis_title").format(day))

                    # Save and close the figure
                    chart_filename = os.path.join(charts_folder, str(day) + ".png")
                    plt.savefig(chart_filename, bbox_inches = "tight")
                    plt.close()
                    print(f"Chart created at {chart_filename}.")

                # Compute the voice activation average
                if voice_activation_occurrences > 0:
                    voice_activation_average = voice_activation_total / voice_activation_occurrences

                # Write the report
                report_filename = os.path.join(self.output_folder, "report.md")
                with open(report_filename, "w") as report_file:
                    def output_report_line(string, extra_new_line = True):
                        report_file.write(string + "\n")
                        if extra_new_line:
                            report_file.write("\n")

                    # Introduction
                    output_report_line("# " + self.get_string("report_introduction_header"))
                    output_report_line(self.get_string("report_introduction_sentence").format(len(days), days[0], days[-1]))

                    # Log lines
                    output_report_line("# " + self.get_string("report_log_lines_header"))
                    output_report_line(self.get_string("report_log_lines_statistics").format(dated_lines_number, dated_lines_not_included_number, undated_lines_number))
                    output_report_line(self.get_string("report_log_lines_first_dated_line"))
                    output_report_line("> " + first_dated_line.line)
                    output_report_line(self.get_string("report_log_lines_last_dated_line"))
                    output_report_line("> " + last_dated_line.line)

                    # Temperature
                    output_report_line("# " + self.get_string("report_temperature_header"))
                    if temperature_minimum == None:
                        output_report_line(self.get_string("report_temperature_no_data"))
                    else:
                        output_report_line(self.get_string("report_temperature_statistics").format(temperature_minimum, temperature_maximum, temperature_average))

                    # Voice activation
                    output_report_line("# " + self.get_string("report_voice_activation_header"))
                    output_report_line(self.get_string("report_voice_activation_statistics").format(voice_activation_occurrences))
                    if voice_activation_occurrences > 0:
                        output_report_line("* " + self.get_string("report_voice_activation_minimum_duration").format(timedelta_to_string(voice_activation_minimum, self.lang)), False)
                        output_report_line("* " + self.get_string("report_voice_activation_maximum_duration").format(timedelta_to_string(voice_activation_maximum, self.lang)), False)
                        output_report_line("* " + self.get_string("report_voice_activation_average_duration").format(timedelta_to_string(voice_activation_average, self.lang)))

                    # IPs
                    output_report_line("# " + self.get_string("report_ips_header"))
                    output_report_line(self.get_string("report_ips_statistics").format(sum(len(ips[ip_type]) for ip_type in LogLine.IPType)))
                    for ip_type in LogLine.IPType:
                        output_report_line("* " + self.get_string("report_ips_description").format(len(ips[ip_type]), ip_type.description(self.lang)), False)
                        for ip, occurrences in ips[ip_type].items():
                            output_report_line("   * " + self.get_string("report_ips_occurrences").format(ip.ljust(15), occurrences), False)
                    output_report_line("", False)

                    # Phone numbers
                    output_report_line("# " + self.get_string("report_phone_numbers_header"))
                    output_report_line(self.get_string("report_phone_numbers_statistics").format(len(phone_numbers)))
                    longest_phone_number = max(len(phone_number) for phone_number in phone_numbers.keys())
                    for phone_number, phone_number_occurrences in phone_numbers.items():
                        output_report_line("* " + self.get_string("report_phone_numbers_occurrences").format(phone_number.ljust(longest_phone_number + 1), phone_number_occurrences), False)

                # Print a message
                print(f"Report created at {report_filename}.")
            # Else, there is no day included in the analysis
            else:
                if dated_log_line_found:
                    print("Dated log lines were found, but they do not appear to fit within the time range you specified. Please check that the values passed to --first or --last aren't too restrictive.")
                else:
                    print("No dated log lines were found. Please check that the input folder contains log files of a RB800 device with .txt extensions.")
                print("No report or charts were created.")
        finally:
            # Remove the daily logs folder
            if os.path.exists(daily_logs_folder):
                shutil.rmtree(daily_logs_folder)

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(
        description = "Analyse log files of a RB800 device (sold by the italian company Innova), creating a report and charts of the device activity. The log files and lines do not need to be in chronological order. The script requires at most as much disk space as the combined sizes of all log files (in the temporary folder), and usually a few additional MB of disk space (in the output folder)."
    )
    parser.add_argument("input_folder", type = str, help = "folder containing the log files with .txt extensions")
    parser.add_argument("output_folder", type = str, help = "folder where the report and charts are created")
    parser.add_argument("-l", "--lang", dest = "lang", type = str, choices=STRINGS["languages"], default = "en", help = f"language to use for the report and charts (defaults to \"en\")")
    parser.add_argument("--first", dest = "first_day", type = datetime.date.fromisoformat, default = None, help = "first day to include in the analysis, formatted as YYYY-MM-DD (defaults to the first day appearing in the log files)")
    parser.add_argument("--last", dest = "last_day", type = datetime.date.fromisoformat, default = None, help = "last day to include in the analysis, formatted as YYYY-MM-DD (defaults to the last day appearing in the log files)")
    parser.add_argument("--tmp", dest = "temporary_folder", type = str, default = None, help = "folder where temporary files used to perform the analysis are created, and deleted when the analysis is over (defaults to the output folder)")
    arguments = parser.parse_args()

    # Create and run the analysis
    log_analysis = LogAnalysis(arguments.input_folder, arguments.output_folder, arguments.lang, arguments.first_day, arguments.last_day, arguments.temporary_folder)
    log_analysis.run()